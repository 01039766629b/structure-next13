import { AxiosResponse } from 'axios';

import ApiAxios                  from '@/core/axios/api.axios';
import { SampleResponseModel }   from '@/domain/sample/model/sample-response.model';
import { SampleRequestModel }    from '@/domain/sample/model/sample-request.model';
import { PageableResponseModel } from '@/core/model/pageable/pageable-response.model';
import { Validate }              from '@/core/decorator/validate.decorator';

export class SampleApi {

  private http: ApiAxios = new ApiAxios();

  getList() {

    return this.http
      .get('/artists/page')
      .then((response: AxiosResponse<PageableResponseModel.Page<SampleResponseModel.FindAll>>) => response.data);
  }

  getOne(id: number) {

    return this.http
      .get(`/artists/${id}`)
      .then((response: AxiosResponse<SampleResponseModel.FindOne>) => response.data);
  }

  @Validate()
  onAdd(params: SampleRequestModel.Add) {

    return this.http
      .post(`/artists`, params)
      .then((response: AxiosResponse<SampleResponseModel.FindOne>) => response.data);
  }

  @Validate()
  onModify(params: SampleRequestModel.Modify) {

    return this.http
      .put(`/artists/${params.id}`, params)
      .then((response: AxiosResponse<SampleResponseModel.FindOne>) => response.data);
  }

  onDelete(id: number) {

    return this.http
      .delete(`artists/${id}`)
  }
}