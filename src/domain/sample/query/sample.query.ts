import { QueryClient }    from '@tanstack/react-query';
import { useQuery }       from '@tanstack/react-query';
import { useMutation }    from '@tanstack/react-query';
import { useQueryClient } from '@tanstack/react-query';

import Mapper                    from '@/core/service/mapper.service';
import Container                 from '@/core/container';
import { SampleApi }             from '@/domain/sample/api/sample.api';
import { SampleResponseModel }   from '@/domain/sample/model/sample-response.model';
import { SampleRequestModel }    from '@/domain/sample/model/sample-request.model';
import { PageableResponseModel } from '@/core/model/pageable/pageable-response.model';

const SampleQuery = () => {

  const sampleApi  : SampleApi   = new SampleApi();
  const mapper     : Mapper      = Container.resolve(Mapper);
  const queryClient: QueryClient = useQueryClient();

  const getList = () => {

    return useQuery<PageableResponseModel.Page<SampleResponseModel.FindAll>>({
        queryKey: ['sample-list'],
        queryFn: () => sampleApi.getList(),
        select: (response: PageableResponseModel.Page<SampleResponseModel.FindAll>) => mapper.toPage(SampleResponseModel.FindAll, response)
      });
  }

  const getOne = (id: number) => {

    return useQuery<SampleResponseModel.FindOne>({
      queryKey: ['sample-detail'],
      queryFn: () => sampleApi.getOne(id),
      select: (response: SampleResponseModel.FindOne) => mapper.toObject(SampleResponseModel.FindOne, response)
    });
  }

  const onAdd = () => {

    return useMutation({
      mutationFn: (params: SampleRequestModel.Add) => {
        const add = mapper.toObject(SampleRequestModel.Add, params);

        return sampleApi.onAdd(add);
      },
      onSuccess: () => {

        queryClient.invalidateQueries({ queryKey: ['sample-list'] });
      }
    })
  }

  const onModify = () => {

    return useMutation({
      mutationFn: (params: SampleRequestModel.Modify) => {
        const modify = mapper.toObject(SampleRequestModel.Modify, params);

        return sampleApi.onModify(modify);
      },
      onSuccess: () => {

        queryClient.invalidateQueries({ queryKey: ['sample-list'] });
      }
    })
  }

  const onDelete = () => {

    return useMutation({
      mutationFn: (id: number) => sampleApi.onDelete(id)
    })
  }

  return {
    getList,
    getOne,
    onAdd,
    onModify,
    onDelete
  }
}

export default SampleQuery;