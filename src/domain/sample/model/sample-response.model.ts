import { Attribute }           from '@/core/decorator/attribute.decorator';
import { Reference }           from '@/core/decorator/reference.decorator';
import { AuthorResponseModel } from '@/core/model/author/author-response.model';
import { SampleEnum }          from '@/domain/sample/enum/sample.enum';

export namespace SampleResponseModel {

  export class FindAll extends AuthorResponseModel.Author {

    @Attribute('아티스트 아이디')
    id!: number;

    @Attribute('이름')
    name!: string;

    @Attribute('설명')
    description!: string;

    @Attribute('소속사이름')
    agencyName!: string;

    @Attribute('출생일')
    birthDate!: string;

    @Attribute('설명')
    body!: string;

    @Attribute('국적')
    @Reference(() => SampleEnum)
    nationalityType!: SampleEnum;
  }

  export class FindOne extends AuthorResponseModel.Author {

    @Attribute('아티스트 아이디')
    id!: number;

    @Attribute('이름')
    name!: string;

    @Attribute('설명')
    description!: string;

    @Attribute('소속사이름')
    agencyName!: string;

    @Attribute('출생일')
    birthDate!: string;

    @Attribute('설명')
    body!: string;

    @Attribute('국적')
    @Reference(() => SampleEnum)
    nationalityType!: SampleEnum;
  }
}