import { Attribute }  from '@/core/decorator/attribute.decorator';
import { IsString }   from '@/core/decorator/validator/type/is-string.decorator';
import { IsNotEmpty } from '@/core/decorator/validator/common/is-not-empty.decorator';
import { Reference }  from '@/core/decorator/reference.decorator';
import { SampleEnum } from '@/domain/sample/enum/sample.enum';

export namespace SampleRequestModel {

  export class Add {

    @Attribute('이름')
    @IsString() @IsNotEmpty()
    name!: string;

    @Attribute('설명')
    @IsString() @IsNotEmpty()
    description!: string;

    @Attribute('소속사이름')
    @IsString() @IsNotEmpty()
    agencyName!: string;

    @Attribute('출생일')
    @IsString() @IsNotEmpty()
    birthDate!: string;

    @Attribute('국적')
    @Reference(() => SampleEnum)
    @IsString() @IsNotEmpty()
    nationalityType!: SampleEnum;
  }

  export class Modify {

    @Attribute('아티스트 아이디')
    id!: number;

    @Attribute('이름')
    @IsString() @IsNotEmpty()
    name!: string;

    @Attribute('설명')
    @IsString() @IsNotEmpty()
    description!: string;

    @Attribute('소속사이름')
    @IsString() @IsNotEmpty()
    agencyName!: string;

    @Attribute('출생일')
    @IsString() @IsNotEmpty()
    birthDate!: string;

    @Attribute('국적')
    @Reference(() => SampleEnum)
    @IsString() @IsNotEmpty()
    nationalityType!: SampleEnum;
  }
}