import {useRouter} from 'next/router';
import {useForm}   from 'react-hook-form';

import Input                 from '@/shared/component/input';
import SampleQuery           from '@/domain/sample/query/sample.query';
import {SampleRequestModel}  from '@/domain/sample/model/sample-request.model';

const SampleAdd = () => {

  const { register, handleSubmit } = useForm<SampleRequestModel.Add>({
    defaultValues: new SampleRequestModel.Add()
  })


  const router   = useRouter();
  const addQuery = SampleQuery().onAdd();

  const onSubmit = (add: SampleRequestModel.Add) => {

    addQuery.mutate(add, {

      onSuccess: (response: any) => {
        if (!!response) {
          router.push('/sample');
        }
      }
    });
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Input label={'이름'}
             className={'input-w328'}
             placeholder={'내용을 입력해 주세요.'}
             register={ register('name') } />

      <Input label={'설명'}
             className={'input-w328'}
             placeholder={'내용을 입력해 주세요.'}
             register={ register('description') } />

      <Input label={'출생일'}
             className={'input-w328'}
             placeholder={'내용을 입력해 주세요.'}
             register={ register('birthDate') } />

      <Input label={'소속사이름'}
             className={'input-w328'}
             placeholder={'내용을 입력해 주세요.'}
             register={ register('agencyName') } />

      <Input label={'국적'}
             className={'input-w328'}
             placeholder={'내용을 입력해 주세요.'}
             register={ register('nationalityType') } />

      <button className={'btn-dark-green-lg'} onClick={() => onSubmit}>등록</button>
    </form>
  )
};

export default SampleAdd;