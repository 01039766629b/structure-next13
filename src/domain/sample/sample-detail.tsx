import SampleQuery from '@/domain/sample/query/sample.query';

type Props = {
  id: number,
}
const SampleDetail = ({ id }: Props) => {

  const { data: detail } = SampleQuery().getOne(id);

  return detail && (
    <ul>
      <li>
        이름: { detail.name }
      </li>
      <li>
        설명: { detail.description }
      </li>
      <li>
        출생일: { detail.birthDate }
      </li>
      <li>
        소속사이름: { detail.agencyName }
      </li>
      <li>
        국적: { detail.nationalityType }
      </li>
      <li>
        등록자: { detail.createdBy }
      </li>
      <li>
        등록일시: { detail.createdAt }
      </li>
    </ul>
  )
}

export default SampleDetail;