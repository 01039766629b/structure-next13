import { useForm } from 'react-hook-form';
import { router }  from 'next/client';

import Input                   from '@/shared/component/input';
import SampleQuery             from '@/domain/sample/query/sample.query';
import { SampleRequestModel }  from '@/domain/sample/model/sample-request.model';
import { SampleResponseModel } from '@/domain/sample/model/sample-response.model';

type Props = {
  detail?: SampleResponseModel.FindOne;
}

const SampleModify = ({detail}: Props) => {

  const { register, handleSubmit } = useForm<SampleRequestModel.Modify>({
    defaultValues: detail
  })

  const onModify = SampleQuery().onModify();

  const onSubmit = (modify: SampleRequestModel.Modify) => {

    onModify.mutate(modify, {

      onSuccess: () => {
        router.push('/sample');
      }
    })
  }

  return (
    detail &&
    <form onSubmit={handleSubmit(onSubmit)}>
      <Input label={'이름'}
             className={'input-w328'}
             placeholder={'내용을 입력해 주세요.'}
             register={ register('name') } />

      <Input label={'설명'}
             className={'input-w328'}
             placeholder={'내용을 입력해 주세요.'}
             register={ register('description') } />

      <Input label={'출생일'}
             className={'input-w328'}
             placeholder={'내용을 입력해 주세요.'}
             register={ register('birthDate') } />

      <Input label={'소속사이름'}
             className={'input-w328'}
             placeholder={'내용을 입력해 주세요.'}
             register={ register('agencyName') } />

      <Input label={'국적'}
             className={'input-w328'}
             placeholder={'내용을 입력해 주세요.'}
             register={ register('nationalityType') } />

      <button className={'btn-red-lg'} onClick={() => onSubmit}>수정</button>
    </form>
  )
};

export default SampleModify;
