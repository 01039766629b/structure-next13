import Link from 'next/link';

import styles      from '@/domain/sample/style/sample-list.module.scss';
import SampleQuery from '@/domain/sample/query/sample.query';

const SampleList = () => {

  const { data: lists } = SampleQuery().getList();

  return (
    <div className={styles.sampleList}>
      <table>
        <thead>
        <tr>
          <th>번호</th>
          <th>이름</th>
          <th>소속사</th>
          <th>설명</th>
          <th>출생일</th>
          <th>국적</th>
          <th>등록자</th>
          <th>등록일시</th>
        </tr>
        </thead>
        <tbody>
          { lists?.content.map((list, index) => {

            return (
              <Link href={`./sample/detail/${list.id}`}
                    key={list.id}
                    legacyBehavior>
                <tr>
                  <td>{ index +1 }</td>
                  <td>{ list.name }</td>
                  <td>{ list.agencyName }</td>
                  <td>{ list.description }</td>
                  <td>{ list.birthDate }</td>
                  <td>{ list.nationalityType.description }</td>
                  <td>{ list.createdBy }</td>
                  <td>{ list.createdAt }</td>
                </tr>
              </Link>
            )
          }) }
        </tbody>
      </table>
    </div>
  )
}

export default SampleList;