import { EnumAbstract } from '@/core/enum';
import { Enumerable }   from '@/core/enum';

@Enumerable
export class SampleEnum extends EnumAbstract {
  static readonly REPUBLIC_OF_KOREA = new SampleEnum('한국');
  static readonly USA               = new SampleEnum('미국');
  static readonly NORTH_KOREA       = new SampleEnum('북한');
  static readonly JAPAN             = new SampleEnum('일본');
}