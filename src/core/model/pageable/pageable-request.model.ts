import { Attribute } from '@/core/decorator/attribute.decorator';

export namespace PageableRequestModel {

  export class Search {
    @Attribute('')
    page: number = 0;

    @Attribute('')
    size!: number;

    @Attribute('')
    sort!: string;

    constructor(options?: PageableRequestModel.Options) {
      this.size = 10;
      this.sort = 'id,desc';

      if (!!options) {
        if (options.size !== null && options.size !== undefined) {
          this.size = options.size;
        }

        if (options.sort !== null && options.sort !== undefined) {
          this.sort = options.sort;
        }
      }
    }
  }

  export class Options {
    @Attribute('')
    size?: number;

    @Attribute('')
    sort?: string;
  }
}