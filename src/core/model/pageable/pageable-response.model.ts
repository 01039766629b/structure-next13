import { Attribute } from '@/core/decorator/attribute.decorator';
import { Reference } from '@/core/decorator/reference.decorator';

export namespace PageableResponseModel {
  export class Page<T> {

    @Attribute('first')
    first!: boolean;

    @Attribute('last')
    last!: boolean;

    @Attribute('number')
    number!: number;

    @Attribute('numberOfElements')
    numberOfElements!: number;

    @Attribute('size')
    size!: number;

    @Attribute('totalElements')
    totalElements!: number;

    @Attribute('totalPages')
    totalPages!: number;

    @Attribute('pageable')
    @Reference(() => PageableResponseModel.PageableInfo)
    pageable!: PageableResponseModel.PageableInfo;

    @Attribute('sort')
    @Reference(() => PageableResponseModel.Sort)
    sort!: PageableResponseModel.Sort;

    @Attribute('목록')
    content!: T[];
  }

  export class PageableInfo {

    @Attribute('offset')
    offset!: number;

    @Attribute('pageNumber')
    pageNumber!: number;

    @Attribute('pageSize')
    pageSize!: number;

    @Attribute('paged')
    paged!: boolean;

    @Attribute('unpaged')
    unpaged!: boolean;

    @Attribute('sort')
    @Reference(() => PageableResponseModel.Sort)
    sort!: PageableResponseModel.Sort;
  }

  export class Sort {

    @Attribute('sorted')
    sorted!: boolean;

    @Attribute('unsorted')
    unsorted!: boolean;
  }
}