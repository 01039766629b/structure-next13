import { Attribute } from '@/core/decorator/attribute.decorator';
import { Reference } from '@/core/decorator/reference.decorator';

export namespace AuthorResponseModel {

  export class Author {
    @Attribute('등록일시')
    createdAt!: string;

    @Attribute('등록자')
    createdBy!: string;

    @Attribute('수정일시')
    updatedAt!: string;

    @Attribute('수정자')
    @Reference(() => AuthorResponseModel.Account)
    updator: AuthorResponseModel.Account = new AuthorResponseModel.Account();
  }

  export class Account {
    @Attribute('사용자일련번호')
    id!: number;

    @Attribute('이름')
    name!: string;

    @Attribute('이메일')
    email!: string;
  }
}