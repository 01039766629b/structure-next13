import { ClassConstructor } from 'class-transformer';
import { plainToInstance }  from 'class-transformer';
import { instanceToPlain }  from 'class-transformer';

import { PageableResponseModel } from '@/core/model/pageable/pageable-response.model';
import { Injectable }            from '@/core/decorator/tsyringe';

@Injectable()
export default class Mapper {

  toObject<T>(type: ClassConstructor<T>, source: T): T {

    return plainToInstance(type, source, {excludeExtraneousValues: true, exposeDefaultValues: true});
  }

  toArray<T>(type: ClassConstructor<T>, source: T[]): T[] {

    return plainToInstance(type, source, {excludeExtraneousValues: true, exposeDefaultValues: true});
  }

  toPage<T>(type: ClassConstructor<T>, source: PageableResponseModel.Page<T>): PageableResponseModel.Page<T> {
    const page: PageableResponseModel.Page<T> = plainToInstance(PageableResponseModel.Page, source, {excludeExtraneousValues: true}) as PageableResponseModel.Page<T>;
    page.content = plainToInstance(type, source.content, {excludeExtraneousValues: true}) as T[];

    return page;
  }

  toPlain<T>(source: T): Record<string, any> {

    return instanceToPlain(source);
  }
}