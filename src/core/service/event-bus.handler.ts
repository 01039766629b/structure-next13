export enum EVENT_BUS {
  TOAST_MESSAGE = 'TOAST_MESSAGE',
}

interface Subscriber {
  event: EVENT_BUS;
  callback: (...args: any[]) => void;
}

export interface EventBusCallback {
  unsubscribe: () => null;
}

class EventBusHandler {
  static instance: EventBusHandler;

  static getInstance(): EventBusHandler {
    if (!this.instance) {
      this.instance = new EventBusHandler();
    }

    return this.instance;
  }

  private subscribers: Map<string, Subscriber> = new Map<string, Subscriber>();

  private constructor() {

  }

  dispatch(event: EVENT_BUS, ...args: any[]) {
    if (!EVENT_BUS[event]) {
      console.warn(`EventBusHandler event(${event}) not found!`);

      return
    }

    if (!this.isExist(event)) {
      console.warn(`EventBusHandler dispatch event(${event}) not found!`);

      return;
    }

    this.subscribers.forEach((subscriber : Subscriber) => {
      if (subscriber.event === event) {
        subscriber.callback(...args);
      }
    })
  }

  subscribe(event: EVENT_BUS, callback: (...args: any[]) => void): EventBusCallback | null {
    if (!EVENT_BUS[event]) {
      console.warn(`EventBusHandler event(${event}) not found!`);

      return null;
    }

    const id: string = Math.random().toString(16);
    const eventBusSubscriber: Subscriber = {
      event,
      callback
    };

    this.subscribers.set(id, eventBusSubscriber);

    return {
      unsubscribe: (): null => {
        this.subscribers.delete(id);

        return null;
      }
    };
  }

  private isExist(event: EVENT_BUS): boolean {
    let returnValue: boolean = false;

    this.subscribers.forEach((value: Subscriber) => {

      if (value.event === event) {
        returnValue = true;
      }
    });

    return returnValue;
  }
}

export const EventBus: EventBusHandler = EventBusHandler.getInstance();