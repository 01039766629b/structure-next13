import { TransformFnParams } from 'class-transformer';
import { DateTimeFormatter } from '@js-joda/core';
import { LocalDateTime }     from '@js-joda/core';
import { LocalDate }         from '@js-joda/core';
import { LocalTime }         from '@js-joda/core';

import { EnumAbstract } from '@/core/enum';

interface DateFormat {
  toPlain?: string;
  toClass?: string;
}

export class ReferenceService {

  getDateFormat(target: any, propertyKey: string): DateFormat {

    return {
      toPlain: Reflect.getMetadata('$dateToString', target, propertyKey),
      toClass: Reflect.getMetadata('$stringToDate', target, propertyKey)
    };
  }

  toEnum(value: TransformFnParams, enums: typeof EnumAbstract) {

    const params = value.obj[value.key];

    if (typeof params === 'string') {

      return enums.valueOf(params);
    }

    if (Array.isArray(params)) {
      const returnArray: any[] = [];

      params.forEach((param) => {
        let pushed: any = param;

        if (typeof param ==='string') {
          pushed = enums.valueOf(param);
        }

        returnArray.push(pushed);
      });

      return returnArray;
    }

    return params;
  }

  fromEnum(value: TransformFnParams) {

    const params = value.obj[value.key];

    if (this.isEnumInstance(params)) {

      return params.key;
    }

    if (Array.isArray(params)) {
      const returnArray: any[] = [];

      params.forEach((param) => {
        let pushed: any = param;

        if (this.isEnumInstance(param)) {
          pushed = param.key;
        }

        returnArray.push(pushed);
      });

      return returnArray;
    }

    return params;
  }

  toLocalDate(value: TransformFnParams, format?: string): any {
    const params = value.obj[value.key];

    if (typeof params ==='string') {
      if (!!format) {

        return LocalDate.parse(params, DateTimeFormatter.ofPattern(format));
      }

      return LocalDate.parse(params);
    }

    if (Array.isArray(params)) {
      const returnArray: any[] = [];

      params.forEach((param) => {
        let pushed: any = param;

        if (typeof param ==='string') {
          if (!!format) {

            pushed = LocalDate.parse(param, DateTimeFormatter.ofPattern(format));
          }

          pushed = LocalDate.parse(param);
        }

        returnArray.push(pushed);
      });

      return returnArray;
    }

    return params;
  }

  toLocalDateTime(value: TransformFnParams, format?: string): any {

    const params = value.obj[value.key];

    if (typeof params ==='string') {
      if (!!format) {

        return LocalDateTime.parse(params, DateTimeFormatter.ofPattern(format));
      }

      return LocalDateTime.parse(params);
    }

    if (Array.isArray(params)) {
      const returnArray: any[] = [];

      params.forEach((param) => {
        let pushed: any = param;

        if (typeof param ==='string') {
          if (!!format) {

            pushed = LocalDateTime.parse(param, DateTimeFormatter.ofPattern(format));
          }

          pushed = LocalDateTime.parse(param);
        }

        returnArray.push(pushed);
      });

      return returnArray;
    }

    return params;
  }

  toLocalTime(value: TransformFnParams, format?: string): any {

    const params = value.obj[value.key];

    if (typeof params ==='string') {
      if (!!format) {

        return LocalTime.parse(params, DateTimeFormatter.ofPattern(format));
      }

      return LocalTime.parse(params);
    }

    if (Array.isArray(params)) {
      const returnArray: any[] = [];

      params.forEach((param) => {
        let pushed: any = param;

        if (typeof param ==='string') {
          if (!!format) {

            pushed = LocalTime.parse(param, DateTimeFormatter.ofPattern(format));
          }

          pushed = LocalTime.parse(param);
        }

        returnArray.push(pushed);
      });

      return returnArray;
    }

    return params;
  }

  fromStringDate(value: TransformFnParams, format?: string): any {

    const params = value.obj[value.key];

    if (params instanceof LocalDateTime) {
      if (!!format) {

        return params.format(DateTimeFormatter.ofPattern(format));
      }

      return params.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    if (params instanceof LocalDate) {
      if (!!format) {

        return params.format(DateTimeFormatter.ofPattern(format));
      }

      return params.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    if (params instanceof LocalTime) {
      if (!!format) {

        return params.format(DateTimeFormatter.ofPattern(format));
      }

      return params.format(DateTimeFormatter.ISO_LOCAL_TIME);
    }

    if (Array.isArray(params)) {
      const returnArray: any[] = [];

      params.forEach((param) => {
        let pushed: any = param;

        if (param instanceof LocalDateTime) {
          if (!!format) {
            pushed = param.format(DateTimeFormatter.ofPattern(format));
          } else {
            pushed = param.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
          }
        }

        if (param instanceof LocalDate) {
          if(!!format) {
            pushed = param.format(DateTimeFormatter.ofPattern(format));
          } else {
            pushed = param.format(DateTimeFormatter.ISO_LOCAL_DATE);
          }
        }

        if (param instanceof LocalTime) {
          if (!!format) {
            pushed = param.format(DateTimeFormatter.ofPattern(format));
          } else {
            pushed = param.format(DateTimeFormatter.ISO_LOCAL_TIME);
          }
        }

        returnArray.push(pushed);
      });

      return returnArray;
    }

    return params;
  }

  isEnumInstance(value: any): boolean {

    return !!value && Object.getPrototypeOf(value) instanceof EnumAbstract;
  }
}