import {EVENT_BUS, EventBus} from '@/core/service/event-bus.handler';
import {ToastModel}          from '@/shared/model/toast.model';

export class EventBusService {

  showToasts(title: string, contents: string | string[]) {
    this.dispatch(EVENT_BUS.TOAST_MESSAGE, new ToastModel.Message({
      title,
      contents: (typeof contents === 'string') ? [contents] : contents
    }));
  }

  private dispatch(event: EVENT_BUS, ...args: any[]) {

    EventBus.dispatch(event, ...args);
  }
}