import { AxiosAbstract } from '@/core/axios/axios.abstract';

export default class ApiAxios extends AxiosAbstract {

  constructor() {
    super(`${process.env.NEXT_PUBLIC_API_URL}`);
  }
}
