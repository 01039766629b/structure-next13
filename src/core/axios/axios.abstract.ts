import Axios                  from 'axios';
import qs                     from 'qs';
import { AxiosInstance }      from 'axios';
import { AxiosResponse }      from 'axios';
import { AxiosRequestConfig } from 'axios';

export abstract class AxiosAbstract {
  axios!: AxiosInstance;

  protected constructor(baseURL: string) {

    this.axios = Axios.create({
      baseURL,
      timeout: 1000 * 60,
    })
  }

  get(url: string, options?: AxiosRequestConfig): Promise<AxiosResponse> {

    return this.axios.get(url, {
        paramsSerializer: (data: any) => qs.stringify(data, {encode: false, allowDots: true, arrayFormat: 'repeat'}),
        ...options
      }
    );
  }

  delete(url: string, options?: AxiosRequestConfig): Promise<AxiosResponse> {

    return this.axios.delete(url, {
        paramsSerializer: (data: any) => qs.stringify(data, {encode: false, allowDots: true, arrayFormat: 'repeat'}),
        ...options
      }
    );
  }

  post(url: string, params?: any, options?: AxiosRequestConfig): Promise<AxiosResponse> {

    return this.axios.post(url, params, {
        paramsSerializer: (data: any) => qs.stringify(data, {encode: false, allowDots: true}),
        ...options
      }
    );
  }

  put(url: string, params?: any, options?: AxiosRequestConfig): Promise<AxiosResponse> {

    return this.axios.put(url, params, {
        paramsSerializer: (data: any) => qs.stringify(data, {encode: false, allowDots: true}),
        ...options
      }
    );
  }

  patch(url: string, params?: any, options?: AxiosRequestConfig): Promise<AxiosResponse> {

    return this.axios.patch(url, params, {
        paramsSerializer: (data: any) => qs.stringify(data, {encode: false, allowDots: true}),
        ...options
      }
    );
  }
}