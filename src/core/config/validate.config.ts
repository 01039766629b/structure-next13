export interface ValidateConfig {
  ORDER: ValidateEnum[];
  MESSAGES: Message;
  VALIDATE_MESSAGE: ValidateMessage;
}

export enum ValidateEnum {
  isNotEmpty    = 'isNotEmpty',
  arrayNotEmpty = 'arrayNotEmpty',

  isString       = 'isString',
  isNumber       = 'isNumber',
  isInt          = 'isInt',
  isBoolean      = 'isBoolean',
  isNumberString = 'isNumberString',
  isArray        = 'isArray',
  isHexColor     = 'isHexColor',
  isEnum         = 'isEnum',

  length             = 'length',
  max                = 'max',
  min                = 'min',
  minLength          = 'minLength',
  maxLength          = 'maxLength',
  maxLengthWithEmoji = 'maxLengthWithEmoji',

  isValid    = 'isValid',
  isNotValid = 'isNotValid',

  isRelativeUrl = 'isRelativeUrl',
  isAbsoluteUrl = 'isAbsoluteUrl',
  isI18n        = 'isI18n',
  isUserName    = 'isUserName',
  isPhone       = 'isPhone',
  isPostCode    = 'isPostCode',
}

export type Message = {
  [K in ValidateEnum]: string;
};

export type ValidateMessage = {
  [K in string]: string;
};

export const VALIDATE_CONFIG: ValidateConfig = {
  ORDER           : [
    ValidateEnum.isNotEmpty,

    ValidateEnum.isString,
    ValidateEnum.isNumber,
    ValidateEnum.isInt,
    ValidateEnum.isBoolean,
    ValidateEnum.isNumberString,
    ValidateEnum.isArray,
    ValidateEnum.isHexColor,
    ValidateEnum.isEnum,

    ValidateEnum.arrayNotEmpty,

    ValidateEnum.max,
    ValidateEnum.min,
    ValidateEnum.length,
    ValidateEnum.minLength,
    ValidateEnum.maxLength,
    ValidateEnum.maxLengthWithEmoji,

    ValidateEnum.isValid,
    ValidateEnum.isNotValid,

    ValidateEnum.isRelativeUrl,
    ValidateEnum.isAbsoluteUrl,
    ValidateEnum.isI18n,
    ValidateEnum.isUserName,
    ValidateEnum.isPhone,
    ValidateEnum.isPostCode,
  ],
  MESSAGES        : {
    isNotEmpty: '필수 정보 입니다.',

    isBoolean     : '자료형이 참거짓 형태여야 합니다.',
    isString      : '자료형이 문자 형태여야 합니다.',
    isNumber      : '자료형이 숫자 형태여야 합니다.',
    isInt         : '자료형이 숫자 형태여야 합니다.',
    isNumberString: '자료형이 숫자문자 형태여야 합니다.',
    isArray       : '자료형이 배열 형태여야 합니다.',
    isHexColor    : '자료형이 색상 형태여야 합니다.',
    isEnum        : '자료형이 코드 형태여야 합니다.',

    arrayNotEmpty: '배열에 데이터가 필수 정보 입니다.',

    max               : '최대값($constraint1)을 넘을수 없습니다.',
    min               : '최소값($constraint1)을 넘을수 없습니다.',
    length            : '입력 범위($constraint1자 ~ $constraint2자) 사이를 입력 하세요.',
    minLength         : '최소 입력 범위를 초과 하였습니다.',
    maxLength         : '최대 입력 범위를 초과 하였습니다.',
    maxLengthWithEmoji: '최대 입력 범위를 초과 하였습니다.',

    isValid   : '유효성 검사에 실패했습니다.',
    isNotValid: '유효성 검사에 실패했습니다.',

    isAbsoluteUrl: 'URL은 "http://", "https://"를 포함하고 있어야 합니다.',
    isRelativeUrl: 'URL은 영문소문자, 숫자, _(underline), -(hyphen), .(period)으로만 구성되어야합니다.',
    isI18n       : '',
    isUserName   : '이름은 한글, 영문으로만 입력해 주세요.',
    isPhone      : '휴대폰번호를 정확이 입력해 주세요.',
    isPostCode   : '우편번호를 정확이 입력해 주세요.',
  },
  VALIDATE_MESSAGE: {}
};

