import { IsInt as ClassValidatorIsInt, ValidationOptions } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function IsInt(validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = Object.assign({
      message: VALIDATE_CONFIG.MESSAGES.isInt
    }, validationOptions ?? {});

    ClassValidatorIsInt(validationOptions)(target, propertyKey);
  };
}
