import { IsBoolean as ClassValidatorIsBoolean, ValidationOptions } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function IsBoolean(validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = Object.assign({
      message: VALIDATE_CONFIG.MESSAGES.isBoolean
    }, validationOptions ?? {});

    ClassValidatorIsBoolean(validationOptions)(target, propertyKey);
  };
}
