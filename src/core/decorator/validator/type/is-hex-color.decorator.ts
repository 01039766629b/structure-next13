import { IsHexColor as ClassValidatorIsHexColor, ValidationOptions } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function IsHexColor(validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = Object.assign({
      message: VALIDATE_CONFIG.MESSAGES.isHexColor
    }, validationOptions ?? {});

    ClassValidatorIsHexColor(validationOptions)(target, propertyKey);
  };
}
