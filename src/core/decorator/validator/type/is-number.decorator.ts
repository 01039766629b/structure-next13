import { IsNumber as ClassValidatorIsNumber, ValidationOptions } from 'class-validator';
import { IsNumberOptions }                                       from 'class-validator/types/decorator/typechecker/IsNumber';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function IsNumber(options?: IsNumberOptions, validationOptions?: ValidationOptions) {
  return (target: any, propertyKey: string) => {
    validationOptions = Object.assign({
      message: VALIDATE_CONFIG.MESSAGES.isNumber
    }, validationOptions ?? {});

    ClassValidatorIsNumber(options, validationOptions)(target, propertyKey);
  };
}
