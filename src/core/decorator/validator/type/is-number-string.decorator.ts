import ValidatorJS                                                           from 'validator';
import { IsNumberString as ClassValidatorIsNumberString, ValidationOptions } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function IsNumberString(options?: ValidatorJS.IsNumericOptions, validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = Object.assign({
      message: VALIDATE_CONFIG.MESSAGES.isNumberString
    }, validationOptions ?? {});

    ClassValidatorIsNumberString(options, validationOptions)(target, propertyKey);
  };
}
