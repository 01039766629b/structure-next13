import { IsString as ClassValidatorIsString, ValidationOptions } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function IsString(validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = Object.assign({
      message: VALIDATE_CONFIG.MESSAGES.isString
    }, validationOptions ?? {});

    ClassValidatorIsString(validationOptions)(target, propertyKey);
  };
}
