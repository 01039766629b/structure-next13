import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function IsEnum(enumInstance: any, validationOptions?: ValidationOptions) {

  return (target: object, propertyName: string) => {
    registerDecorator({
      target     : target.constructor,
      propertyName,
      options    : validationOptions,
      constraints: [enumInstance],
      validator  : IsEnumValidate,
    });
  };
}

// TODO: 커스텀 해야함.
@ValidatorConstraint({name: 'isEnum'})
export class IsEnumValidate implements ValidatorConstraintInterface {

  validate(target: any, args: ValidationArguments): boolean {
    let returnValue: boolean = false;

    if (!!args.value) {
      // TODO: 인스턴스 자체로 확인해야한다.
      if (args.value.constructor.name === args.constraints[0].name) {
        returnValue = true;
      }
    }

    return returnValue;
  }

  defaultMessage(args: ValidationArguments) {

    let returnValue: string = VALIDATE_CONFIG.MESSAGES.isEnum as string;

    if (!!args.value) {
      // TODO: 인스턴스 자체로 확인해야한다.
      if (args.value.constructor.name !== args.constraints[0].name) {
        returnValue = VALIDATE_CONFIG.VALIDATE_MESSAGE.isEnumNotEquals;
      }
    }

    return returnValue;
  }
}
