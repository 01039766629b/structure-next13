import { ArrayNotEmpty as ClassValidatorArrayNotEmpty, ValidationOptions } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function ArrayNotEmpty(validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = Object.assign({
      message: VALIDATE_CONFIG.MESSAGES.arrayNotEmpty
    }, validationOptions ?? {});

    ClassValidatorArrayNotEmpty(validationOptions)(target, propertyKey);
  };
}
