import { IsNotEmpty, registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { Exclude }                                                                                                                  from 'class-transformer';
import { ExposeOptions }                                                                                                            from 'class-transformer/types/interfaces';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function IsValid(options?: { expose?: ExposeOptions, validation?: ValidationOptions }) {

  return (target: any, propertyKey: string) => {
    Exclude({toPlainOnly: true})(target, propertyKey);
    IsNotEmpty()(target, propertyKey);
    IsValidDecorator(options?.validation)(target, propertyKey);
  };
}

function IsValidDecorator(validationOptions?: ValidationOptions) {

  return (target: object, propertyName: string) => {
    registerDecorator({
      target     : target.constructor,
      propertyName,
      options    : validationOptions,
      constraints: [],
      validator  : IsValidValidate,
    });
  };
}

@ValidatorConstraint({name: 'isValid'})
class IsValidValidate implements ValidatorConstraintInterface {

  validate(validator: () => boolean, args: ValidationArguments): boolean {

    return validator();
  }

  defaultMessage(args: ValidationArguments): string {

    return VALIDATE_CONFIG.MESSAGES.isValid;
  }
}
