import { IsNotEmpty, registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { Exclude }                                                                                                                  from 'class-transformer';
import { ExposeOptions }                                                                                                            from 'class-transformer/types/interfaces';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function IsNotValid(options?: { expose?: ExposeOptions, validation?: ValidationOptions }) {

  return (target: any, propertyKey: string) => {
    Exclude({toPlainOnly: true})(target, propertyKey);
    IsNotEmpty()(target, propertyKey);
    IsNotValidDecorator(options?.validation)(target, propertyKey);
  };
}

function IsNotValidDecorator(validationOptions?: ValidationOptions) {

  return (target: object, propertyName: string) => {
    registerDecorator({
      target     : target.constructor,
      propertyName,
      options    : validationOptions,
      constraints: [],
      validator  : IsNotValidValidate,
    });
  };
}

@ValidatorConstraint({name: 'isNotValid'})
class IsNotValidValidate implements ValidatorConstraintInterface {

  validate(validator: () => boolean, args: ValidationArguments): boolean {

    return !validator();
  }

  defaultMessage(args: ValidationArguments): string {

    return VALIDATE_CONFIG.MESSAGES.isNotValid;
  }
}
