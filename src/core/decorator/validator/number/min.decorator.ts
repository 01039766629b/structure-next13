import { Min as ClassValidatorMin, ValidationOptions } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function Min(minValue: number, validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = Object.assign({
      message: VALIDATE_CONFIG.MESSAGES.min
    }, validationOptions ?? {});

    ClassValidatorMin(minValue, validationOptions)(target, propertyKey);
  };
}
