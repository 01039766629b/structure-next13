import { Max as ClassValidatorMax, ValidationOptions } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function Max(maxValue: number, validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {

    validationOptions = Object.assign({
      message: VALIDATE_CONFIG.MESSAGES.max
    }, validationOptions ?? {});

    ClassValidatorMax(maxValue, validationOptions)(target, propertyKey);
  };
}
