import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function IsUserName(validationOptions?: ValidationOptions) {

  return (target: object, propertyName: string) => {
    registerDecorator({
      target     : target.constructor,
      propertyName,
      options    : validationOptions,
      constraints: [],
      validator  : IsUserNameValidate,
    });
  };
}

@ValidatorConstraint({name: 'isUserName'})
class IsUserNameValidate implements ValidatorConstraintInterface {

  validate(url: string, args: ValidationArguments): boolean {
    let returnValue: boolean = false;

    if (!!url) {
      returnValue = (/^[가-힣|a-z|A-Z|]+$/).test(url);
    }

    return returnValue;
  }

  defaultMessage(args: ValidationArguments): string {

    return VALIDATE_CONFIG.MESSAGES.isUserName;
  }
}
