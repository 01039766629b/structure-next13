import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function IsAbsoluteUrl(validationOptions?: ValidationOptions) {

  return (target: object, propertyName: string) => {
    registerDecorator({
      target     : target.constructor,
      propertyName,
      options    : validationOptions,
      constraints: [],
      validator  : IsAbsoluteUrlValidate,
    });
  };
}

@ValidatorConstraint({name: 'isAbsoluteUrl'})
class IsAbsoluteUrlValidate implements ValidatorConstraintInterface {

  validate(url: string, args: ValidationArguments): boolean {
    let returnValue: boolean = false;

    if (!!url) {
      returnValue = (/^https?:\/\/(.*)$/).test(url);
    }

    return returnValue;
  }

  defaultMessage(args: ValidationArguments): string {

    return VALIDATE_CONFIG.MESSAGES.isAbsoluteUrl;
  }
}
