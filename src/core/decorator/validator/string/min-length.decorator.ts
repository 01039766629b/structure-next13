import { MinLength as ClassValidatorMinLength, ValidationOptions } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function MinLength(minValue: number, validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = Object.assign({
      message: VALIDATE_CONFIG.MESSAGES.minLength
    }, validationOptions ?? {});

    ClassValidatorMinLength(minValue, validationOptions)(target, propertyKey);
  };
}
