import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function IsPostCode(validationOptions?: ValidationOptions) {

  return (target: object, propertyName: string) => {
    registerDecorator({
      target     : target.constructor,
      propertyName,
      options    : validationOptions,
      constraints: [],
      validator  : IsPostCodeValidate,
    });
  };
}

@ValidatorConstraint({name: 'isPostCode'})
class IsPostCodeValidate implements ValidatorConstraintInterface {

  validate(url: string, args: ValidationArguments): boolean {
    let returnValue: boolean = false;

    if (!!url) {
      returnValue = (/^([0-9]{5,6})$/).test(url);
    }

    return returnValue;
  }

  defaultMessage(args: ValidationArguments): string {

    return VALIDATE_CONFIG.MESSAGES.isPostCode;
  }
}
