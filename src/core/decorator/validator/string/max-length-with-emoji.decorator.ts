import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function MaxLengthWithEmoji(

  max: number,
  validationOptions?: ValidationOptions
) {
  return (target: object, propertyName: string) => {
    registerDecorator({
      target     : target.constructor,
      propertyName,
      options    : validationOptions,
      constraints: [max],
      validator  : MaxLengthWithEmojiValidate,
    });
  };
}

@ValidatorConstraint({name: 'maxLengthWithEmoji'})
export class MaxLengthWithEmojiValidate implements ValidatorConstraintInterface {

  validate(value: string, args: ValidationArguments): boolean {
    let returnValue: boolean = false;

    if (!!value && value.length <= args.constraints[0]) {
      returnValue = true;
    }

    return returnValue;
  }

  defaultMessage(args: ValidationArguments): string {

    return VALIDATE_CONFIG.MESSAGES.maxLengthWithEmoji;
  }
}
