import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function IsPhone(validationOptions?: ValidationOptions) {

  return (target: object, propertyName: string) => {
    registerDecorator({
      target     : target.constructor,
      propertyName,
      options    : validationOptions,
      constraints: [],
      validator  : IsPhoneValidate,
    });
  };
}

@ValidatorConstraint({name: 'isPhone'})
class IsPhoneValidate implements ValidatorConstraintInterface {

  validate(url: string, args: ValidationArguments): boolean {
    let returnValue: boolean = false;

    if (!!url) {
      returnValue = (/^01([016789]?)([1-9]{1}[0-9]{2,3})([0-9]{4})$/).test(url);
    }

    return returnValue;
  }

  defaultMessage(args: ValidationArguments): string {

    return VALIDATE_CONFIG.MESSAGES.isPhone;
  }
}
