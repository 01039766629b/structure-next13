import { MaxLength as ClassValidatorMaxLength, ValidationOptions } from 'class-validator';

import { VALIDATE_CONFIG } from '@/core/config/validate.config';

export function MaxLength(maxValue: number, validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = Object.assign({
      message: VALIDATE_CONFIG.MESSAGES.maxLength
    }, validationOptions ?? {});

    ClassValidatorMaxLength(maxValue, validationOptions)(target, propertyKey);
  };
}
