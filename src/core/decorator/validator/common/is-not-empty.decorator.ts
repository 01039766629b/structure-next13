import { IsNotEmpty as ClassValidatorIsNotEmpty, ValidationOptions } from 'class-validator';

import {VALIDATE_CONFIG} from '@/core/config/validate.config';

export function IsNotEmpty(validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = Object.assign({
      message: VALIDATE_CONFIG.MESSAGES.isNotEmpty
    }, validationOptions ?? {});

    ClassValidatorIsNotEmpty(validationOptions)(target, propertyKey);
  };
}
