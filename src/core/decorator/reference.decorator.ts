import { LocalDate }         from '@js-joda/core';
import { LocalDateTime }     from '@js-joda/core';
import { LocalTime }         from '@js-joda/core';
import { TypeOptions }       from 'class-transformer/types/interfaces';
import { TransformFnParams } from 'class-transformer';
import { Type }              from 'class-transformer';
import { Transform }         from 'class-transformer';

import { EnumAbstract }     from '@/core/enum';
import { ReferenceService } from '@/core/service/reference.service';

const referenceService: ReferenceService = new ReferenceService();

export function Reference(typeFunction: any, typeOptions?: TypeOptions) {
  return (target: any, propertyKey: string) => {

    if (!!typeFunction()) {
      switch (typeFunction()) {
        case LocalDate:
          const localDateFormat = referenceService.getDateFormat(target, propertyKey);
          Type(() => typeFunction().now, typeOptions)(target, propertyKey);
          Transform((value: TransformFnParams) => referenceService.toLocalDate(value, localDateFormat.toClass), {toClassOnly: true})(target, propertyKey);
          Transform((value: TransformFnParams) => referenceService.fromStringDate(value, localDateFormat.toPlain), {toPlainOnly: true})(target, propertyKey);
          break;

        case LocalDateTime:
          const localDateTimeFormat = referenceService.getDateFormat(target, propertyKey);
          Type(() => typeFunction().now, typeOptions)(target, propertyKey);
          Transform((value: TransformFnParams) => referenceService.toLocalDateTime(value, localDateTimeFormat.toClass), {toClassOnly: true})(target, propertyKey);
          Transform((value: TransformFnParams) => referenceService.fromStringDate(value, localDateTimeFormat.toPlain), {toPlainOnly: true})(target, propertyKey);
          break;

        case LocalTime:
          const localTimeFormat = referenceService.getDateFormat(target, propertyKey);
          Type(() => typeFunction().now, typeOptions)(target, propertyKey);
          Transform((value: TransformFnParams) => referenceService.toLocalTime(value, localTimeFormat.toClass), {toClassOnly: true})(target, propertyKey);
          Transform((value: TransformFnParams) => referenceService.fromStringDate(value, localTimeFormat.toPlain), {toPlainOnly: true})(target, propertyKey);
          break;

        default:
          switch (Object.getPrototypeOf(typeFunction())) {
            case EnumAbstract:
              Type(typeFunction, typeOptions)(target, propertyKey);
              Transform((value: TransformFnParams) => referenceService.toEnum(value, typeFunction()), {toClassOnly: true})(target, propertyKey);
              Transform((value: TransformFnParams) => referenceService.fromEnum(value), {toPlainOnly: true})(target, propertyKey);
              break;

            default:
              Type(typeFunction, typeOptions)(target, propertyKey);
              break;
          }
          break;
      }
    } else {
      Type(typeFunction, typeOptions)(target, propertyKey);
    }
  }
}
