import { container } from 'tsyringe';

export default class Container {
  static resolve<T>(params: any): T {

    return container.resolve(params);
  }
}
