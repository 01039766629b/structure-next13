import type { AppProps }       from 'next/app'
import { QueryClient }         from '@tanstack/react-query';
import { QueryClientProvider } from '@tanstack/react-query';

import 'reflect-metadata';
import '@/core/style/main.scss'
import Toast                   from '@/shared/component/toast';

export default function App({ Component, pageProps }: AppProps) {

  const queryClient = new QueryClient()

  return (
    <QueryClientProvider client={queryClient}>
      <Component {...pageProps} />
      <Toast />
    </QueryClientProvider>
  )
}
