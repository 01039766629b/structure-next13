import SampleModify from '@/domain/sample/sample-modify';
import {useRouter}  from 'next/router';
import SampleQuery  from '@/domain/sample/query/sample.query';

const SampleModifyPage = () => {

  const router = useRouter();

  const { data: detail } = SampleQuery().getOne(Number(router.query.id));

  return (
    detail && <SampleModify detail={detail} />
  )
};

export default SampleModifyPage;