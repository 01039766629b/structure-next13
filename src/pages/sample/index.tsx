import Link from 'next/link';

import SampleList from '@/domain/sample/sample-list';

const SampleListPage = () => {

  return (
    <>
      <SampleList />

      <Link href={'/sample/add'}>
        <button className={'btn-green-lg'}>등록</button>
      </Link>

    </>
  )
}

export default SampleListPage;
