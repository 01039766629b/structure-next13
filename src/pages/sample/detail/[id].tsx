import Link           from 'next/link';
import { useRouter }  from 'next/router';

import SampleDetail   from '@/domain/sample/sample-detail';
import SampleQuery    from '@/domain/sample/query/sample.query';

const SampleDetailPage = () => {

  const router      = useRouter();
  const deleteQuery = SampleQuery().onDelete();

  const onDelete = () => {

    deleteQuery.mutate(Number(router.query.id), {

      onSuccess: () => {
        router.push('/sample');
      }
    });
  }

  return (
    <>
      {router.query.id && <SampleDetail id={Number(router.query.id)}/>}
      <Link href={`/sample/modify/${router.query.id}`}>
        <button className={'btn-red-lg'}>수정</button>
      </Link>

      <button className={'btn-gray-lg'} onClick={() => onDelete()}>삭제</button>
    </>
  )
};

export default SampleDetailPage;
