import Head from 'next/head'

import SampleListPage from '@/pages/sample';

export default function Home() {

  return (
    <>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="structure-next13" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <SampleListPage />
      </main>
    </>
  )
}
