import { useEffect } from 'react';
import { useState }  from 'react';

import styles               from '@/shared/component/style/toast.module.scss';
import { EventBusCallback } from '@/core/service/event-bus.handler';
import { EVENT_BUS }        from '@/core/service/event-bus.handler';
import { ToastModel }       from '@/shared/model/toast.model';
import { EventBus }         from '@/core/service/event-bus.handler';

const Toast = () => {

  const [ onToastState, setOnToastState ] = useState(false);
  const [ error, setError               ] = useState<ToastModel.Message>(new ToastModel.Message);
  let eventBus: EventBusCallback | null = null;

  const onToast = (error: ToastModel.Message) => {

    setOnToastState(true);
    setError(error);
  }

  useEffect(() => {

    if (onToastState) {
      setTimeout(() => {

        setOnToastState(false);

        if (!!eventBus) {
          eventBus = eventBus.unsubscribe();
        }
      }, 3000)
    }
  }, [onToastState]);

  useEffect(() => {

    eventBus = EventBus.subscribe(EVENT_BUS.TOAST_MESSAGE, (error) => onToast(error));
  }, []);

  return (
    onToastState &&
    <div className={styles.toast}>
      <h4>{error.title}</h4>
      {error.contents.map(item => {

        return <p key={item}>{item}</p>
    })}
    </div>
  )
};

export default Toast;