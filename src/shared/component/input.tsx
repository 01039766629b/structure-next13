import { UseFormRegisterReturn } from 'react-hook-form';

type Props = {
  label?: string;
  className?: string;
  placeholder?: string;
  register: UseFormRegisterReturn<string>;
}

const Input = ({ label, register, className, placeholder }: Props) => {

  return (
    <>
    <label className={'label'}>{label}</label>
    <input className={className}
           placeholder={placeholder}
           type="text" {...register} />
    </>
  )
};

export default Input;