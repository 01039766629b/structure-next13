export namespace ToastModel {

  export class Message {
    title!: string;
    contents!: string[];

    constructor(options?: { title?: string; contents?: string[]; }) {

      if (!!options) {
        if (!!options.title) {
          this.title = options.title;
        }

        if (!!options.contents) {
          this.contents = options.contents;
        }
      }
    }
  }
}